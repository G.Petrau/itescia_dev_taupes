<?php
require 'vendor/autoload.php';
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Firebase\JWT\JWT;
$app = new \Slim\App;
error_reporting(E_ALL);
ini_set('display_errors', 1);
define("HOSTNAME", "localhost");
define("DBNAME", "projet_singe");
define("USER", "hearthstone");
define("PASSWORD", "1234");
session_start();



$app->get('/user', function(Request $request, Response $response){
  $tb = $request->getQueryParams();
  $login = $tb["login"];
  $mdp = $tb["mdp"];

  return $response->withStatus(200)
    ->withHeader('Content-type', 'application/json')
    ->write(checkUser($login, $mdp));
});


$app->post('/user', function(Request $request, Response $response){
  $email = $request->getParsedBody()['email'];
  $nom = $request->getParsedBody()['nom'];
  $prenom = $request->getParsedBody()['prenom'];
  $mdp = $request->getParsedBody()['mdp'];
  $idUtil = $request->getParsedBody()['type'];
  $idEcole = $request->getParsedBody()['idEcole'];

  return insertUser($nom, $prenom, $email, $mdp, $idUtil, $idEcole);
});


$app->get('/classementEcole/{id}', function(Request $request, Response $response){
  $idEcole = $request->getAttribute('id');

  return $response->withStatus(200)
    ->withHeader('Content-type', 'application/json')
    ->write(getClassementEcole($idEcole));
});


$app->get('/classement', function(Request $request, Response $response){
  return $response->withStatus(200)
    ->withHeader('Content-type', 'application/json')
    ->write(getClassement());
});


$app->get('/ecole', function(Request $request, Response $response){
  return $response->withStatus(200)
    ->withHeader('Content-type', 'application/json')
    ->write(getEcole());
});


$app->get('/fiche/{id}', function(Request $request, Response $response){
  $idFiche = $request->getAttribute('id');
  return $response->withStatus(200)
    ->withHeader('Content-type', 'application/json')
    ->write(getFiche($idFiche));
});


$app->post('/score/{valeur}', function(Request $request, Response $response){
  $valeur = $request->getAttribute('valeur');
  $id = $_SESSION['id'];

  return $response->withStatus(200)
    ->withHeader('Content-type', 'application/json')
    ->write(updateScore($id, $valeur));
});


$app->get('/revisions', function(Request $request, Response $response){
  return $response->withStatus(200)
    ->withHeader('Content-type', 'application/json')
    ->write(getAllTest());
});


$app->post('/revisions', function(Request $request, Response $response){
  $fields = $request->getParsedBody();

  // echo "<pre><code>";
  // print_r($fields);
  // echo "</code></pre>";

  return $response->withStatus(200)
    ->withHeader('Content-type', 'application/json')
    ->write(insertRevision($fields));
});



function insertRevision($data) {
  $libelle = $data['libelle'];
  $sql = "INSERT INTO fiche_revision(libelle) VALUES(:libelle)";
  $bdd = getBdd();
  $rverif = $bdd->prepare($sql);
  $rverif->bindParam(":libelle", $libelle);
  $rverif->execute();
  $affectedRows = $rverif->rowCount();
  
  if ($affectedRows == 1) {
    $idFiche = $bdd->lastInsertId();

    foreach ($data['questions'] as $question) {
      insertQuestion($idFiche, $question);
    }

    return $affectedRows == 1 ? "true" : "false";
  }

  return "false : ".$affectedRows;
}


function insertQuestion($id, $data) {
  $libelle = $data['libelle'];
  $sql = "INSERT INTO operation(libelle, idFiche) VALUES(:libelle, :id)";
  $bdd = getBdd();
  $rverif = $bdd->prepare($sql);
  $rverif->bindParam(":libelle", $libelle);
  $rverif->bindParam(":id", $id);
  $rverif->execute();
  $affectedRows = $rverif->rowCount();
  
  if ($affectedRows == 1) {
    $idQuestion = $bdd->lastInsertId();

    foreach ($data['reponses'] as $reponse) {
      insertResponse($idQuestion, $reponse['text']);
    }

    return $affectedRows == 1 ? "true" : "false";
  }

  return "false";
}


function insertResponse($idQuestion, $libelle) {
  $sql = "INSERT INTO fiche_reponse(libelle, idOperation) VALUES(:libelle, :idQuestion)";
  $bdd = getBdd();
  $rverif = $bdd->prepare($sql);
  $rverif->bindParam(":libelle", $libelle);
  $rverif->bindParam(":idQuestion", $idQuestion);
  $rverif->execute();
  $affectedRows = $rverif->rowCount();

  return $affectedRows == 1 ? "true" : "false";
}


function getAllTest()
{
  $bdd = getBdd();
  $sql="SELECT * FROM fiche_revision";
  $rverif = $bdd->prepare($sql);
  $rverif->execute();
  $result = $rverif->fetchAll(PDO::FETCH_ASSOC);

  return json_encode($result);
}


function updateScore($id, $valeur)
{
  $bdd = getBdd();
  $sql="UPDATE utilisateur SET score = score + :valeur WHERE id=:id";
  $rverif = $bdd->prepare($sql);
  $rverif->bindParam(":valeur", $valeur);
  $rverif->bindParam(":id", $id);
  $rverif->execute();
  $rows = $rverif->rowCount();

  return json_encode($rows == 1 ? "true" : "false");
}


function getFiche($idFiche)
{
  $bdd = getBdd();
  $sql="SELECT fiche_revision.id, fiche_revision.libelle as libelle_rev, operation.id AS id_op, operation.libelle as calcul, fiche_reponse.id AS id_rep, fiche_reponse.libelle
  FROM operation
  INNER JOIN fiche_revision ON fiche_revision.id = operation.idFiche
  INNER JOIN fiche_reponse ON fiche_reponse.idOperation = operation.id
  AND fiche_revision.id = :idFiche
  ORDER BY fiche_revision.id, operation.id, id_rep";
  $rverif = $bdd->prepare($sql);
  $rverif->bindParam(":idFiche", $idFiche);
  $rverif->execute();
  $result = $rverif->fetchAll(PDO::FETCH_ASSOC);

  $array = [];
  $arrayUniqueId = [];

  // on recup un array pour chaque question
  foreach ($result as $value) {
    $arrayUniqueId[] = ["id" => $value['id_op'], "libelle" => $value['calcul'], "responses" => [] ];
  }

  // on supprime les doublons
  $arrayUniqueId = array_unique($arrayUniqueId, SORT_REGULAR);
  foreach ($arrayUniqueId as $value) {
    $array[] = $value;
  }

  // et on ajoute les réponses
  foreach ($result as $value) {
    for ($i = 0; $i < count($array); $i++) { 
      if ($array[$i]['id'] == $value['id_op']) {
        $array[$i]['responses'][] = ["id" => $value['id_rep'], "text" => $value['libelle']];
      }
    }
  }

  return json_encode([
    "id" => $value['id'],
    "libelle" => $value['libelle_rev'],
    "questions" => $array
  ]);
}


function getEcole()
{
  $bdd = getBdd();
  $sql="SELECT nom, id FROM ecole";
  $rverif = $bdd->prepare($sql);
  $rverif->execute();
  $result = $rverif->fetchAll(PDO::FETCH_ASSOC);

  return json_encode($result);
}


function checkUser($login, $mdp)
{
  $sql = "SELECT * FROM utilisateur WHERE email=:login AND mdp=:mdp";
  $bdd = getBdd();
  $rverif = $bdd->prepare($sql);
  $rverif->bindParam(":login", $login);
  $rverif->bindParam(":mdp", $mdp);
  $rverif->execute();
  $result = $rverif->fetch(PDO::FETCH_ASSOC);
  $_SESSION = $result;
  $affectedRows = $rverif->rowCount();

  return $affectedRows == 1 ? "true" : "false";

}


function insertUser($nom, $prenom, $email, $mdp, $idUtil, $idEcole)
{
  $sql = "INSERT INTO utilisateur(nom, prenom, email, login, mdp, idUtil, idEcole) VALUES(:nom, :prenom, :email, 'dontcare', :mdp, :idUtil, :idEcole)";
  $bdd = getBdd();
  $rverif = $bdd->prepare($sql);
  $rverif->bindParam(":nom", $nom);
  $rverif->bindParam(":prenom", $prenom);
  $rverif->bindParam(":email", $email);
  $rverif->bindParam(":mdp", $mdp);
  $rverif->bindParam(":idUtil", $idUtil);
  $rverif->bindParam(":idEcole", $idEcole);
  $rverif->execute();
  $affectedRows = $rverif->rowCount();
  
  return $affectedRows == 1 ? "true" : "false";
}


function getClassementEcole($idEcole)
{
  $bdd = getBdd();
  $sql="SELECT nom, prenom, score FROM utilisateur WHERE idEcole=:idEcole ";
  $rverif = $bdd->prepare($sql);
  $rverif->bindParam(":idEcole", $idEcole);
  $rverif->execute();
  $result = $rverif->fetchAll(PDO::FETCH_ASSOC);
  
  return json_encode($result);
}


function getClassement()
{
  $bdd = getBdd();
  $sql="SELECT nom, prenom, score FROM utilisateur ORDER BY score DESC";
  $rverif = $bdd->prepare($sql);
  $rverif->execute();
  $result = $rverif->fetchAll(PDO::FETCH_ASSOC);

  return json_encode($result);
}


function getBdd()
{
  try
  {
    $bdd = new PDO('mysql:host='.HOSTNAME.';dbname='.DBNAME, USER, PASSWORD);
    return $bdd;
  }
  catch (PDOException $e)
  {
    echo 'Erreur : ' .$e->getMessage();
    echo 'N° ' .$e->getCode();
  }
}


function getTokenJWT($login, $pass)
{
  // Make an array for the JWT Payload
  $payload = array(
    //60 min
    "exp" => time() + (60 * 30),
    "data" => array(
      "login" => $login,
      "pass" => $pass
    )
  );
  // encode the payload using our secretkey and return the token
  return JWT::encode($payload, "promise", "HS256");
}


function validJWT($token)
{
  try {
    $decoded = JWT::decode($token, "promise", array('HS256'));
    return checkUser($decoded->data->login, $decoded->data->pass);
  } catch (Exception $e){
    return false;
  }
}


$app->run();
