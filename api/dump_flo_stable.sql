-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.24 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.5.0.5332
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour projet_singe
CREATE DATABASE IF NOT EXISTS `projet_singe` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `projet_singe`;

-- Listage de la structure de la table projet_singe. classement
CREATE TABLE IF NOT EXISTS `classement` (
  `idUtil` int(11) NOT NULL,
  `idRevision` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  KEY `FK_classement_utilisateur` (`idUtil`),
  KEY `FK_classement_fiche_revision` (`idRevision`),
  CONSTRAINT `FK_classement_fiche_revision` FOREIGN KEY (`idRevision`) REFERENCES `fiche_revision` (`id`),
  CONSTRAINT `FK_classement_utilisateur` FOREIGN KEY (`idUtil`) REFERENCES `utilisateur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table projet_singe.classement : ~0 rows (environ)
/*!40000 ALTER TABLE `classement` DISABLE KEYS */;
/*!40000 ALTER TABLE `classement` ENABLE KEYS */;

-- Listage de la structure de la table projet_singe. ecole
CREATE TABLE IF NOT EXISTS `ecole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Listage des données de la table projet_singe.ecole : ~3 rows (environ)
/*!40000 ALTER TABLE `ecole` DISABLE KEYS */;
INSERT INTO `ecole` (`id`, `nom`) VALUES
	(1, 'Les Jouannes'),
	(2, 'Les Touleuses'),
	(3, 'Chemin-Dupuis');
/*!40000 ALTER TABLE `ecole` ENABLE KEYS */;

-- Listage de la structure de la table projet_singe. fiche_reponse
CREATE TABLE IF NOT EXISTS `fiche_reponse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(50) NOT NULL,
  `idOperation` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_fiche_reponse_operation` (`idOperation`),
  CONSTRAINT `FK_fiche_reponse_operation` FOREIGN KEY (`idOperation`) REFERENCES `operation` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

-- Listage des données de la table projet_singe.fiche_reponse : ~54 rows (environ)
/*!40000 ALTER TABLE `fiche_reponse` DISABLE KEYS */;
INSERT INTO `fiche_reponse` (`id`, `libelle`, `idOperation`) VALUES
	(1, '4', 1),
	(2, '3', 1),
	(3, '5', 1),
	(4, '1', 2),
	(5, '2', 2),
	(6, '5', 2),
	(7, '10', 11),
	(8, '8', 11),
	(9, '9', 11),
	(10, '4', 8),
	(11, '6', 8),
	(12, '5', 8),
	(13, '15', 4),
	(14, '14', 4),
	(15, '16', 4),
	(16, '19', 14),
	(17, '18', 14),
	(18, '16', 14),
	(19, '32', 17),
	(20, '28', 17),
	(21, '34', 17),
	(22, '8', 5),
	(23, '6', 5),
	(24, '7', 5),
	(25, '7', 9),
	(26, '3', 9),
	(27, '2', 9),
	(28, '15', 13),
	(29, '8', 13),
	(30, '20', 13),
	(31, '18', 6),
	(32, '10', 6),
	(33, '9', 6),
	(34, '36', 18),
	(35, '12', 18),
	(36, '40', 18),
	(37, '17', 3),
	(38, '15', 3),
	(39, '14', 3),
	(40, '64', 16),
	(41, '80', 16),
	(42, '48', 16),
	(43, '5', 7),
	(44, '6', 7),
	(45, '4', 7),
	(46, '5', 10),
	(47, '4', 10),
	(48, '3', 10),
	(49, '2', 12),
	(50, '0', 12),
	(51, '1', 12),
	(52, '45', 15),
	(53, '54', 15),
	(54, '60', 15);
/*!40000 ALTER TABLE `fiche_reponse` ENABLE KEYS */;

-- Listage de la structure de la table projet_singe. fiche_revision
CREATE TABLE IF NOT EXISTS `fiche_revision` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `score` int(11) DEFAULT NULL,
  `libelle` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Listage des données de la table projet_singe.fiche_revision : ~3 rows (environ)
/*!40000 ALTER TABLE `fiche_revision` DISABLE KEYS */;
INSERT INTO `fiche_revision` (`id`, `score`, `libelle`) VALUES
	(1, NULL, 'Suite Addition'),
	(2, NULL, 'Suite Soustraction'),
	(3, NULL, 'Suite Multiplication');
/*!40000 ALTER TABLE `fiche_revision` ENABLE KEYS */;

-- Listage de la structure de la table projet_singe. operation
CREATE TABLE IF NOT EXISTS `operation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(50) NOT NULL,
  `idFiche` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_operation_fiche_revision` (`idFiche`),
  CONSTRAINT `FK_operation_fiche_revision` FOREIGN KEY (`idFiche`) REFERENCES `fiche_revision` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Listage des données de la table projet_singe.operation : ~18 rows (environ)
/*!40000 ALTER TABLE `operation` DISABLE KEYS */;
INSERT INTO `operation` (`id`, `libelle`, `idFiche`) VALUES
	(1, '1+3 = ?', 1),
	(2, '3-2 = ?', 2),
	(3, '8+7 = ?', 1),
	(4, '12+3 = ?', 1),
	(5, '5+2 = ?', 1),
	(6, '6+3 = ?', 1),
	(7, '9-4 = ?', 2),
	(8, '10-6 = ?', 2),
	(9, '5-2 = ?', 2),
	(10, '9-6 = ?', 2),
	(11, '12-4 = ?', 2),
	(12, '9-8 = ?', 2),
	(13, '5x3 = ?', 3),
	(14, '2x8 = ?', 3),
	(15, '9x6 = ?', 3),
	(16, '8x8 = ?', 3),
	(17, '4x7 = ?', 3),
	(18, '6x6 = ?', 3);
/*!40000 ALTER TABLE `operation` ENABLE KEYS */;

-- Listage de la structure de la table projet_singe. reponse_eleve
CREATE TABLE IF NOT EXISTS `reponse_eleve` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idEleve` int(11) NOT NULL,
  `idReponse` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_reponse_eleve_eleve` (`idEleve`),
  KEY `FK_reponse_eleve_fiche_reponse` (`idReponse`),
  CONSTRAINT `FK_reponse_eleve_eleve` FOREIGN KEY (`idEleve`) REFERENCES `eleve` (`id`),
  CONSTRAINT `FK_reponse_eleve_fiche_reponse` FOREIGN KEY (`idReponse`) REFERENCES `fiche_reponse` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table projet_singe.reponse_eleve : ~0 rows (environ)
/*!40000 ALTER TABLE `reponse_eleve` DISABLE KEYS */;
/*!40000 ALTER TABLE `reponse_eleve` ENABLE KEYS */;

-- Listage de la structure de la table projet_singe. type_utilisateur
CREATE TABLE IF NOT EXISTS `type_utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Listage des données de la table projet_singe.type_utilisateur : ~2 rows (environ)
/*!40000 ALTER TABLE `type_utilisateur` DISABLE KEYS */;
INSERT INTO `type_utilisateur` (`id`, `libelle`) VALUES
	(1, 'eleve'),
	(2, 'professeur');
/*!40000 ALTER TABLE `type_utilisateur` ENABLE KEYS */;

-- Listage de la structure de la table projet_singe. utilisateur
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `login` varchar(50) NOT NULL,
  `mdp` varchar(50) NOT NULL,
  `idUtil` int(11) NOT NULL,
  `idEcole` int(11) NOT NULL,
  `score` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_utilisateur_ecole` (`idEcole`),
  KEY `FK_utilisateur_type_utilisateur` (`idUtil`),
  CONSTRAINT `FK_utilisateur_ecole` FOREIGN KEY (`idEcole`) REFERENCES `ecole` (`id`),
  CONSTRAINT `FK_utilisateur_type_utilisateur` FOREIGN KEY (`idUtil`) REFERENCES `type_utilisateur` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Listage des données de la table projet_singe.utilisateur : ~5 rows (environ)
/*!40000 ALTER TABLE `utilisateur` DISABLE KEYS */;
INSERT INTO `utilisateur` (`id`, `nom`, `prenom`, `email`, `login`, `mdp`, `idUtil`, `idEcole`, `score`) VALUES
	(1, 'Jean-Michel', 'Dupont', 'd', 'd', 'd', 1, 1, 350),
	(2, 'Xavier', 'Baldane', 'd', 'fg', 'gf', 1, 1, 87),
	(3, 'Axel', 'Silvestre', 'cdd', 'aaa', 'xcf', 1, 2, 14),
	(4, 'Julien', 'Garnier', 'zrr', 'zaa', 'aaa', 1, 2, 56),
	(5, 'Alphonse', 'Baudet', 'aaa', 'ettg', 'erte', 1, 2, 8);
/*!40000 ALTER TABLE `utilisateur` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
