-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.24 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Listage des données de la table projet_singe.ecole : ~3 rows (environ)
/*!40000 ALTER TABLE `ecole` DISABLE KEYS */;
INSERT INTO `ecole` (`id`, `nom`) VALUES
	(1, 'Les Jouannes'),
	(2, 'Les Touleuses'),
	(3, 'Chemin-Dupuis');
/*!40000 ALTER TABLE `ecole` ENABLE KEYS */;

-- Listage des données de la table projet_singe.fiche_reponse : ~4 rows (environ)
/*!40000 ALTER TABLE `fiche_reponse` DISABLE KEYS */;
INSERT INTO `fiche_reponse` (`id`, `libelle`, `idOperation`) VALUES
	(1, '4', 1),
	(2, '3', 1),
	(3, '5', 1),
	(4, '1', 2),
	(5, '2', 2),
	(6, '5', 2),
	(7, '10', 11),
	(8, '8', 11),
	(9, '9', 11),
	(10, '4', 8),
	(11, '6', 8),
	(12, '5', 8),
	(13, '15', 4),
	(14, '14', 4),
	(15, '16', 4),
	(16, '19', 14),
	(17, '18', 14),
	(18, '16', 14),
	(19, '32', 17),
	(20, '28', 17),
	(21, '34', 17),
	(22, '8', 5),
	(23, '6', 5),
	(24, '7', 5),
	(25, '7', 9),
	(26, '3', 9),
	(27, '2', 9),
	(28, '15', 13),
	(29, '8', 13),
	(30, '20', 13),
	(31, '18', 6),
	(32, '10', 6),
	(33, '9', 6),
	(34, '36', 18),
	(35, '12', 18),
	(36, '40', 18),
	(37, '17', 3),
	(38, '15', 3),
	(39, '14', 3),
	(40, '64', 16),
	(41, '80', 16),
	(42, '48', 16),
	(43, '5', 7),
	(44, '6', 7),
	(45, '4', 7),
	(46, '5', 10),
	(47, '4', 10),
	(48, '3', 10),
	(49, '2', 12),
	(50, '0', 12),
	(51, '1', 12),
	(52, '45', 15),
	(53, '54', 15),
	(54, '60', 15);
/*!40000 ALTER TABLE `fiche_reponse` ENABLE KEYS */;

-- Listage des données de la table projet_singe.fiche_revision : ~0 rows (environ)
/*!40000 ALTER TABLE `fiche_revision` DISABLE KEYS */;
INSERT INTO `fiche_revision` (`id`, `score`, `libelle`) VALUES
	(1, NULL, 'Suite Addition'),
	(2, NULL, 'Suite Soustraction'),
	(3, NULL, 'Suite Multiplication');
/*!40000 ALTER TABLE `fiche_revision` ENABLE KEYS */;

-- Listage des données de la table projet_singe.operation : ~2 rows (environ)
/*!40000 ALTER TABLE `operation` DISABLE KEYS */;
INSERT INTO `operation` (`id`, `libelle`, `idFiche`) VALUES
	(1, '1+3 = ?', 1),
	(2, '3-2 = ?', 2),
	(3, '8+7 = ?', 1),
	(4, '12+3 = ?', 1),
	(5, '5+2 = ?', 1),
	(6, '6+3 = ?', 1),
	(7, '9-4 = ?', 2),
	(8, '10-6 = ?', 2),
	(9, '5-2 = ?', 2),
	(10, '9-6 = ?', 2),
	(11, '12-4 = ?', 2),
	(12, '9-8 = ?', 2),
	(13, '5x3 = ?', 3),
	(14, '2x8 = ?', 3),
	(15, '9x6 = ?', 3),
	(16, '8x8 = ?', 3),
	(17, '4x7 = ?', 3),
	(18, '6x6 = ?', 3);
/*!40000 ALTER TABLE `operation` ENABLE KEYS */;

-- Listage des données de la table projet_singe.reponse_eleve : ~0 rows (environ)
/*!40000 ALTER TABLE `reponse_eleve` DISABLE KEYS */;
/*!40000 ALTER TABLE `reponse_eleve` ENABLE KEYS */;

-- Listage des données de la table projet_singe.type_utilisateur : ~2 rows (environ)
/*!40000 ALTER TABLE `type_utilisateur` DISABLE KEYS */;
INSERT INTO `type_utilisateur` (`id`, `libelle`) VALUES
	(1, 'eleve'),
	(2, 'professeur');
/*!40000 ALTER TABLE `type_utilisateur` ENABLE KEYS */;

-- Listage des données de la table projet_singe.utilisateur : ~0 rows (environ)
/*!40000 ALTER TABLE `utilisateur` DISABLE KEYS */;
INSERT INTO `utilisateur` (`id`, `nom`, `prenom`, `email`, `login`, `mdp`, `idUtil`, `idEcole`, `score`) VALUES
	(1, 'Jean-Michel', 'Dupont', 'd', 'd', 'd', 1, 1, 25),
	(2, 'Xavier', 'Baldane', 'd', 'fg', 'gf', 1, 1, 87),
	(3, 'Axel', 'Silvestre', 'cdd', 'aaa', 'xcf', 1, 2, 14),
	(4, 'Julien', 'Garnier', 'zrr', 'zaa', 'aaa', 1, 2, 56),
	(5, 'Alphonse', 'Baudet', 'aaa', 'ettg', 'erte', 1, 2, 8);
/*!40000 ALTER TABLE `utilisateur` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
