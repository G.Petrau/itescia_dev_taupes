<!DOCTYPE html>
<html>
  <head>
    <script src="js/jquery.min.js"></script>  
    <script src="js/bootstrap.min.js"></script>
    <script src="js/sweetalert2.all.min.js"></script> 
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sweetalert2.min.css">
    <title>Page d'inscription</title>
  </head>

  <body>
    <?php include "header.php"; ?>

    
    <!-- # L'enfant pourra s'inscrire en indiquant son nom, son prénom et sa classe et son école pour faire partie du palmarès -->
    <div class="container">
      <h1 class="text-center pt-5 pb-5">Page d'inscription</h1>


      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="email">Email</label>
          <input type="text" class="form-control" id="email" placeholder="Votre email">
        </div>
        <div class="form-group col-md-6">
          <label for="mdp">Mot de passe</label>
          <input type="password" class="form-control" id="mdp" placeholder="Mot de passe">
        </div>
      </div>
      <div class="form-group">
        <label for="nom">Nom</label>
        <input type="text" class="form-control" id="nom" placeholder="Votre nom">
      </div>
      <div class="form-group">
        <label for="prenom">Prénom</label>
        <input type="text" class="form-control" id="prenom" placeholder="Votre prénom">
      </div>
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="ecole">Ecole</label>
          <select id="ecole" class="form-control">
            <option selected disabled>Choisir mon ecole...</option>
          </select>
        </div>
      </div>
      <center class="pt-4"><button type="submit" id="buttonSubmit" class="btn-lg btn-primary">S'inscrire</button></center>

    </div>
    <script>
      $(document).ready(function() {

        $.ajax({
          type: "GET",
          url: "/itescia_dev_taupes/api/ecole",
          success: function(data){
            for (let index = 0; index < data.length; index++) {
              $("#ecole").append(
                "<option value='"+data[index].id+"'>"+data[index].nom+"</option>"
              );
            }
          }
        });



        $("#buttonSubmit").click(function() { 

          $.ajax({
            type: "POST",
            url: "/itescia_dev_taupes/api/user",
            data: {
              email: document.getElementById("email").value,
              mdp: document.getElementById("mdp").value,
              nom: document.getElementById("nom").value,
              prenom: document.getElementById("prenom").value,
              idEcole: document.getElementById("ecole").value,
              type: 1
            },
            success: function(data){
              console.log(data);

              if (data) {
                Swal.fire({
                  title: 'Succès !',
                  text: 'Vous allez etre redirigé vers la page de connexion dans quelques secondes...',
                  timer: 2000,
                  timerProgressBar: true,
                  didOpen: () => {
                    Swal.showLoading()
                  }  
                }).then((result) => {
                  window.location.href = "ConnexionForm";
                });
              }
              else {
                Swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: 'Erreur lors de la création de votre compte...',
                });
              }
            }
          });

        });

      });
    </script>

  </body>
</html>