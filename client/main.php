<!DOCTYPE html>
<html>
  <head>
    <script src="js/jquery.min.js"></script>  
    <script src="js/bootstrap.min.js"></script>
    <script src="js/sweetalert2.all.min.js"></script> 
    <script src="js/app_canvas.js"></script> 
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sweetalert2.min.css">
    <title>Page principale</title>
  </head>

  <body>
    <?php include "header.php"; ?>
    <?php if (!$_SESSION['id']) {
      header("Location: ConnexionForm");
    } ?>

    <div class="container">
      <h1 class="text-center pt-2 pb-2" id="title"></h1>
      <h2 class="text-center pt-5 pb-2" id="titleQuestion"></h2>
      <canvas id="myCanvas" width="1000" height="500"></canvas>
    </div>
  </body>
</html>
