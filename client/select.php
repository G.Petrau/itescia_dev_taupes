<!DOCTYPE html>
<html>
  <head>
    <script src="js/jquery.min.js"></script>  
    <script src="js/bootstrap.min.js"></script>
    <script src="js/sweetalert2.all.min.js"></script>  
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sweetalert2.min.css">
    <title>Page d'inscription</title>
  </head>

  <body>
    <?php include "header.php"; ?>
    <?php if (!$_SESSION['id']) {
      header("Location: ConnexionForm");
    } ?>

    <div class="container">
      <h1 class="text-center pt-5 pb-5">Choix de l'exercice</h1>

      <div class="list-group" id="listLink">

      </div>
    </div>
    <script>
      $(document).ready(function() {

          $.ajax({
            type: "GET",
            url: "/itescia_dev_taupes/api/revisions",
            success: function(data){
              for (let index = 0; index < data.length; index++) {
                $("#listLink").append(
                  "<a class='list-group-item list-group-item-action' href='main?id="+data[index].id+"'>📚 - "+data[index].libelle+"</a>"
                );
              }
            }
          });

        });
    </script>
  </body>
</html>