
    <?php session_start(); ?>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="index">Accueil</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="InscriptionForm">Inscription</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="ConnexionForm">Connexion</a>
          </li>
          <?php if (isset($_SESSION['id'])): ?>
          <li class="nav-item">
            <a class="nav-link" href="select">Jouer</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="Classement">Classement</a>
          </li>
          <?php if ($_SESSION['idUtil'] == 2): ?>
          <li class="nav-item">
            <a class="nav-link" href="createFiche">Créer une fiche</a>
          </li>
          <?php endif; ?>
          <?php endif; ?>
        </ul>
      </div>
      <div class="form-inline my-2 my-lg-0">
        <?php if (isset($_SESSION['id'])): ?>
        <span style="padding-right: 30px;"><?php echo $_SESSION['nom']." ".$_SESSION['prenom']; ?> </span>
        <a class="btn btn-outline-success my-2 my-sm-0" type="button" href="logout">Se déconnecter</a>
        <?php else: ?>
        <a class="btn btn-outline-success my-2 my-sm-0" type="button" href="ConnexionForm">Se connecter</a>
        <?php endif; ?>
      </div>
    </nav>