<!DOCTYPE html>
<html>
  <head>
    <script src="js/jquery.min.js"></script>  
    <script src="js/bootstrap.min.js"></script>
    <script src="js/sweetalert2.all.min.js"></script> 
    <script src="js/createFiche.js"></script> 
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sweetalert2.min.css">
    <title>Page de création de fiche</title>
  </head>

  <body>
    <?php include "header.php"; ?>
    <?php if (!$_SESSION['id']) {
      header("Location: ConnexionForm");
    } ?>

    <div class="container">
      <h1 class="text-center pt-5 pb-5" id="title">Page de création de fiche</h1>


      <div>
        <div class="form-group row">
          <label for="libelleFiche" class="col-sm-2 col-form-label">Libelle de la fiche</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="libelleFiche" name="libelleFiche" placeholder="Exemple : Suite Addition">
          </div>
        </div>
        <fieldset class="form-group">
          <div class="row">
            <legend class="col-form-label col-sm-2 pt-0">Nombre de questions</legend>
            <div class="col-sm-10">
              <div class="form-check">
                <input class="form-check-input" type="radio" name="nbQuestions" id="nbQuestions1" value="1" checked>
                <label class="form-check-label" for="nbQuestions1">
                  1
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="nbQuestions" id="nbQuestions2" value="2">
                <label class="form-check-label" for="nbQuestions2">
                  2
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="nbQuestions" id="nbQuestions3" value="3">
                <label class="form-check-label" for="nbQuestions3">
                  3
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="nbQuestions" id="nbQuestions4" value="4">
                <label class="form-check-label" for="nbQuestions4">
                  4
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="nbQuestions" id="nbQuestions5" value="5">
                <label class="form-check-label" for="nbQuestions5">
                  5
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="nbQuestions" id="nbQuestions6" value="6">
                <label class="form-check-label" for="nbQuestions6">
                  6
                </label>
              </div>
            </div>
          </div>
        </fieldset>
        <fieldset class="form-group">
          <div class="row">
            <legend class="col-form-label col-sm-2 pt-0">Visibilité</legend>
            <div class="col-sm-10">
              <div class="form-check">
                <input class="form-check-input" type="radio" name="visibility" id="visibilityChoice1" value="public" checked>
                <label class="form-check-label" for="visibilityChoice1">
                  Publique (accessible à tous)
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="visibility" id="visibilityChoice2" value="private">
                <label class="form-check-label" for="visibilityChoice2">
                  Privée (accessible uniquement avec un lien internet)
                </label>
              </div>
            </div>
          </div>
        </fieldset>
      </div>

      <hr>
      <h3 class="text-center pt-3 pb-3" id="title">Questions / réponses</h3>
      <div id="questionList"></div>

      <center class="pt-4 pb-5">
        <button type="submit" id="submitForm" class="btn-lg btn-primary">Enregistrer</button>
      </center>

      
    </div>

  </body>
</html>