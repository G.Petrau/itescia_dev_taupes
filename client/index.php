<!DOCTYPE html>
<html>
  <head>
    <script src="js/jquery.min.js"></script>  
    <script src="js/bootstrap.min.js"></script>
    <script src="js/sweetalert2.all.min.js"></script>  
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sweetalert2.min.css">
    <title>Page d'accueil</title>
  </head>

  <body>
    <?php include "header.php"; ?>

    <div class="container">
      <h1 class="text-center pt-5 pb-5">Accueil</h1>
      <div class="pt-5 pb-5">
        <center><img src="img/singe.png"></center>
      </div>
      <h4>
        SingeCalcul est une application qui permet de réviser les tables d'opération (addition, soustraction, multiplication, division). Elle se présente sous forme de jeux pour enfant, l'enfant peux choisir quelle table il doit réviser, une interface graphique s'affichera alors en lui proposant le calcul à effectuer et des petits singes sortiront des trous avec des petits panneaux pour lui proposer des résultats de calculs. A lui de choisir la bonne !
      </h4>
      <center class="pt-4"><a type="submit" id="buttonSubmit" class="btn-lg btn-primary" href="select">Commencer à jouer !</a></center>
    </div>
    
  </body>
</html>