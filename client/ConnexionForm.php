<!DOCTYPE html>
<html>
  <head>
    <script src="js/jquery.min.js"></script>  
    <script src="js/bootstrap.min.js"></script>
    <script src="js/sweetalert2.all.min.js"></script>  
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sweetalert2.min.css">
    <title>Page de connexion</title>
  </head>

  <body>
    <?php include "header.php"; ?>

    
    <!-- # L'enfant pourra s'inscrire en indiquant son nom, son prénom et sa classe et son école pour faire partie du palmarès -->
    <div class="container">
      <h1 class="text-center pt-5 pb-5">Page de connexion</h1>

      <div class="form-group">
        <label for="exampleemailInputEmail1">Email</label>
        <input type="text" class="form-control" id="email" placeholder="Votre email">
        <small id="emailHelp" class="form-text text-muted"></small>
      </div>
      <div class="form-group">
        <label for="mdp">Mot de passe</label>
        <input type="password" class="form-control" id="mdp" placeholder="Votre mot de passe">
      </div>
      <button type="submit" id="buttonSubmit" class="btn btn-primary">Se Connecter</button>

    </div>

    <script>
      $(document).ready(function() {

        $("#buttonSubmit").click(function() { 

          $.ajax({
            type: "GET",
            url: "/itescia_dev_taupes/api/user",
            data: {
              login: document.getElementById("email").value,
              mdp: document.getElementById("mdp").value
            },
            success: function(data){
              console.log(data);

              if (data) {
                Swal.fire({
                  title: 'Succès !',
                  text: 'Vous allez etre redirigé dans quelques secondes...',
                  timer: 2000,
                  timerProgressBar: true,
                  didOpen: () => {
                    Swal.showLoading()
                  }  
                }).then((result) => {
                  window.location.href = "index";
                });
              }
              else {
                Swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: 'Vos identifiants ne sont pas valides !',
                });
              }
            }
          });

        });

      });
    </script>

  </body>
</html>