$( document ).ready(function() {
    $.ajax({
        type: "GET",
        url: "/itescia_dev_taupes/api/classement",
        success: function(data){
            for (let index = 0; index < data.length; index++) {
                $("#leaderboard").append("<tr>").append(
                "<td>"+data[index].nom+"</td>" +
                "<td>"+data[index].prenom+"</td>" +
                "<td>"+data[index].score+"</td>"
                );
            }
            
        }
    });
    $.ajax({
        type: "GET",
        url: "/itescia_dev_taupes/api/ecole",
        success: function(data){
            for (let index = 0; index < data.length; index++) {
    
                $("#ecole").append(
                "<option value='"+data[index].id+"'>"+data[index].nom+"</option>"
                );
            }
            
        }
    });
    $('#ecole').change(function(){
        let id = $("#ecole").val();
        $("#leaderboard").empty();
        if(id == "Global")
        {
            $.ajax({
                type: "GET",
                url: "/itescia_dev_taupes/api/classement",
                success: function(data){
                    for (let index = 0; index < data.length; index++) {
                        $("#leaderboard").append("<tr>").append(
                        "<td>"+data[index].nom+"</td>" +
                        "<td>"+data[index].prenom+"</td>" +
                        "<td>"+data[index].score+"</td>"
                        );
                    }  
                }
            });
        }
        else
        {
            $.ajax({
                type: "GET",
                url: "/itescia_dev_taupes/api/classementEcole/" + id,
                success: function(data){
                    for (let index = 0; index < data.length; index++) {
                            $("#leaderboard").append("<tr>").append(
                            "<td>"+data[index].nom+"</td>" +
                            "<td>"+data[index].prenom+"</td>" +
                            "<td>"+data[index].score+"</td>"
                            );
        
                    }
                }
            })
        }
    });

});