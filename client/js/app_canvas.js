$(document).ready(function() {

  var canvas = document.getElementById("myCanvas");
  var context = canvas.getContext("2d");
  var score = 0;
  var numberError = 0;
  var dataQuery = {};
  var currentIndexQuestion = 0;
  var queryDict = {};
  location.search.substr(1).split("&").forEach(function(item) {queryDict[item.split("=")[0]] = item.split("=")[1]});
  var image = new Image();
  image.src = 'img/singe.png';


  image.onload = function () {
    $.ajax({
      type: "GET",
      url: "/itescia_dev_taupes/api/fiche/" + queryDict.id,
      success: function(data){
        console.log(data);
        dataQuery = data;

        updateGameState();
      }
    });
  }


  function updateGameState() {
    context.clearRect(0, 0, canvas.width, canvas.height);
    $("#title").html(dataQuery.libelle);
    $("#titleQuestion").html(" ");
    $("#myCanvas").unbind();

    console.log("currentIndexQuestion: "+currentIndexQuestion)

    if (dataQuery.questions.length != 0 && dataQuery.questions.length > currentIndexQuestion) {
      $("#titleQuestion").html(dataQuery.questions[currentIndexQuestion].libelle);
      let dx = 30;
      let dy = 30;
  
      dataQuery.questions[currentIndexQuestion].responses.forEach(question => {
        console.log(question.text + " : " + dx + " - " + dy );
  
        drawCanvas(context, question.text, dx, dy, question.id, dataQuery.questions[currentIndexQuestion].libelle);
  
        dx += 297 + 30;
        if ((dataQuery.questions[currentIndexQuestion].responses.indexOf(question) + 1) % 3 == 0) {
          dx = 30;
          dy += 199 + 30;
        }
      });

      currentIndexQuestion++;
    }
    else {
      score = 20 - Math.floor((20 / dataQuery.questions.length) * numberError);

      $.ajax({
        type: "POST",
        url: "/itescia_dev_taupes/api/score/"+score
      });

      if (numberError == 0) {
        Swal.fire({
          title: 'Bravo !',
          text: 'Vous avez réussi votre entrainenement ! Vous gagnez donc '+score+' points !',
          icon: 'success',
        }).then((result) => {
          window.location.href = "select";
        });
      }
      else {
        Swal.fire({
          title: 'Dommage !',
          text: 'Vous avez réussi votre entrainenement mais vous avez fait '+numberError+' fautes ! Vous gagnez donc '+score+' points !',
          icon: 'warning',
        }).then((result) => {
          window.location.href = "select";
        });
      }


    }
  }

  function drawCanvas(ctx, text, dx, dy, id, libelle) {
    let padding = 70 - (text.length * 4);
    ctx.font = "30px Arial";
    ctx.drawImage(image, dx, dy);

    $("#myCanvas").on('mousedown', function(e) {
        getCursorPosition(canvas, e, id, dx, dy, image.width + dx, image.height + dy, text, libelle);
    })
    
    ctx.fillText(text, dx + padding, dy + 60); 
  }

  function getCursorPosition(canvas, event, id, ix, iy, iex, iey, text, libelle) {
    let rect = canvas.getBoundingClientRect();
    let x = event.clientX - rect.left;
    let y = event.clientY - rect.top;
  
    if (x >= ix && x <= iex && y >= iy && y <= iey) {
      console.log("id = " + id + ", { x: " + x + " y: " + y + " }");

      libelle = libelle.replace("x", "*");
      libelle = libelle.replace("=", "");
      libelle = libelle.replace("?", "");
      libelle = libelle.replace(" ", "");
      let somme = eval(libelle);

      if (somme != parseInt(text, 10)) {
        numberError += 1;
      }
      
      updateGameState();
    }
  }

});