$(document).ready(function() {
  var numberOfQuestions = parseInt($("input[name='nbQuestions']:checked").val(), 10);
  var numberOfAnwsers = {};
  CreateFields();
  console.log(numberOfAnwsers);

  $('input[type=radio][name=nbQuestions]').change(function() {
    numberOfAnwsers = {};
    numberOfQuestions = parseInt($("input[name='nbQuestions']:checked").val(), 10);
    $("#questionList").empty();
    CreateFields();
    console.log(numberOfAnwsers);
  });

  $('#submitForm').click(function() {
    let json = { 
      libelle: $("#libelleFiche").val(),
      visibility: $("input[name='visibility']:checked").val(),
      questions: []
    }

    for (let property in numberOfAnwsers) {

      property = parseInt(property, 10);
      json.questions.push({ libelle: $(`#questionLibelle-${property}`).val(), reponses: [] });

      for (let idResponse = 1; idResponse <= numberOfAnwsers[property]; idResponse++) {
        json.questions[property - 1].reponses.push({ text: $(`#textReponse-${property+'-'+idResponse}`).val() });
      }
    }

    $.ajax({
      type: "POST",
      url: "/itescia_dev_taupes/api/revisions",
      data: json
    });

    console.log(json);

    Swal.fire({
      title: 'Enregistrement réussi !',
      text: 'La fiche a bien ajoutée !',
      icon: 'success',
    }).then((result) => {
      window.location.href = "select";
    });
  });

  function CreateFields() {
    for (let idQuestion = 1; idQuestion <= numberOfQuestions; idQuestion++) {


      numberOfAnwsers[idQuestion] = 1;
      $("#questionList").append(getHTMLQuestionBody(idQuestion));
      let nbReponse = parseInt($(`input[name='nbReponse-${idQuestion}']:checked`).val(), 10);


      $(`input[type=radio][name=nbReponse-${idQuestion}]`).change(function() {
        let nbReponse = parseInt($(`input[name='nbReponse-${idQuestion}']:checked`).val(), 10);

        numberOfAnwsers[idQuestion] = nbReponse;
        $(`#reponsesList-${idQuestion}`).empty();
        
        for (let responseId = 1; responseId <= nbReponse; responseId++) {
          $("#reponsesList-"+idQuestion).append(getHTMLResponseBody(idQuestion, responseId));
        }
        console.log(numberOfAnwsers);
      });


      for (let responseId = 1; responseId <= nbReponse; responseId++) {
        $("#reponsesList-"+idQuestion).append(getHTMLResponseBody(idQuestion, responseId));
      }
    }
  }

  function getHTMLQuestionBody(idQuestion) {
    return `
    <div id="questionBody-${idQuestion}">
      <h4>Question ${idQuestion}:</h4>
    
      <div class="form-group row">
        <label for="questionLibelle-" class="col-sm-2 col-form-label">Libelle de la question</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="questionLibelle-${idQuestion}" name="questionLibelle-${idQuestion}" placeholder="Exemple : 2+2 = ?">
        </div>
      </div>
    
      <fieldset class="form-group">
        <div class="row">
          <legend class="col-form-label col-sm-2 pt-0">Nombre de réponses</legend>
          <div class="col-sm-10">
            <div class="form-check">
              <input class="form-check-input" type="radio" name="nbReponse-${idQuestion}" id="nbReponse1-${idQuestion}" value="1" checked>
              <label class="form-check-label" for="nbReponse1-${idQuestion}">
                1
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="nbReponse-${idQuestion}" id="nbReponse2-${idQuestion}" value="2">
              <label class="form-check-label" for="nbReponse2-${idQuestion}">
                2
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="nbReponse-${idQuestion}" id="nbReponse3-${idQuestion}" value="3">
              <label class="form-check-label" for="nbReponse3-${idQuestion}">
                3
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="nbReponse-${idQuestion}" id="nbReponse4-${idQuestion}" value="4">
              <label class="form-check-label" for="nbReponse4-${idQuestion}">
                4
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="nbReponse-${idQuestion}" id="nbReponse5-${idQuestion}" value="5">
              <label class="form-check-label" for="nbReponse5-${idQuestion}">
                5
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="nbReponse-${idQuestion}" id="nbReponse6-${idQuestion}" value="6">
              <label class="form-check-label" for="nbReponse6-${idQuestion}">
                6
              </label>
            </div>
          </div>
        </div>
      </fieldset>
    
      <div id="reponsesList-${idQuestion}"></div>
    </div>`;
  }

  function getHTMLResponseBody(idQuestion, idResponse) {
    return `    
    <div class="form-group row">
      <label for="textReponse-${idQuestion}-${idResponse}" class="col-sm-2 col-form-label">Libelle de la réponse ${idResponse}</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="textReponse-${idQuestion}-${idResponse}" placeholder="Exemple : 4">
      </div>
    </div>`;  
  }

});