# ITESCIA_DEV_TAUPES

## Procédure d'installation

 - Cloner le repository dans le dossier de votre serveur web (il doit y avoir le dossier **itescia_dev_taupes** à la racine de **www** sur votre serveur php apache).
 - Se connecter sur votre base de données **mysql** et charger le fichier sql **api/dump_flo_stable.sql**, normalement il y a une base de données avec comme nom **projet_singe**.
 - Ajouter un utilisateur sql avec les identifiants suivants : **"hearthstone"** avec comme mot de passe **"1234"**, si c'est impossible, vous pouvez changer les identifiants dans le fichier **api/index.php** à la ligne **188**.